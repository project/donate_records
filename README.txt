
Donate Records
--------------
To install, place the entire donate_records folder into your modules directory.
Go to Administer -> Site Building -> Modules and enable donate_records module.

NOTE: Donate module (part of eCommerce module) and Donate Project module are required for Donate Records to run.

Go to Administer -> Settings -> Donate Records and set the 'Donation Term' to the term chosen for your donations.
Set any Mail New Donations settings that you require and save.

To view records of donations, go to Administer -> Logs -> Donate Records.