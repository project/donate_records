if (Drupal.jsEnabled) {
  $(document).ready(function() {
		$("tr").each(function(i) {
			if ($(this).attr('id')) {
				$(this).css('cursor','pointer');
			}
		});
		$("tr").click(function() {
			var id = $(this).attr('id');
			$("#sub"+id).toggle();
		});
  });
}